import { USER_DATA, LOADING_INDICATOR } from '../utils/constant';

const INITIAL_STATE = {
  userData: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
  console.log("ACtion",action)
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };
    case LOADING_INDICATOR:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
};
