import { USER_DATA, LOADING_INDICATOR } from '../utils/constant';
import { loginData } from '../utils/service';

// export const loginUser = userData => {
//   return loginData(userData).then((response) => console.log("API Response", response))
// }

export function loginSuccess(data) {
  return {
      type: USER_DATA,
      payload: data
  }
}

export const loginAction = (data) => {
  return (dispatch) => {
      return loginData(data).then((response) => {
          dispatch(loginSuccess(response.data))
          return Promise.resolve(response)
      }).catch(error => {
          return Promise.reject(error)
      })
  }
};

export const getUserData = userData => {
  return {
    type: USER_DATA,
    payload: userData,
  };
};

export const setUserData = userData => {
  console.log("USER DATA",userData)
  return {
    type: USER_DATA,
    payload: userData,
  };
};

export const setLoadingIndicator = isLoading => {
  return {
    type: LOADING_INDICATOR,
    payload: isLoading,
  };
};
