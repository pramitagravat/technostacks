let URL = "http://demo.ciaoworks.com/practical/login.php"

export const loginData = (data) => {
    return fetch("http://demo.ciaoworks.com/practical/login.php", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(res => res.json()).then(data => { return data });
}