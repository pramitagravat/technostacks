import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../screens/Home'
import Login from '../screens/Login'
import Profile from '../screens/Profile'
import First from '../screens/First';
import Second from '../screens/Second';

const Stack = createStackNavigator();

export default function MyStack() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
                <Stack.Screen name="First" component={First} options={{ headerShown: false }} />
                <Stack.Screen name="Second" component={Second} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
