import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
const Profile = (props) => {
  const [userInfo, setUserInfo] = useState(props.userInfo)
  const [isLoading, setILoading] = useState(true)
  useEffect(() => {
    setILoading(false)
  }, []);


  return (
    <View style={{ flex: 1 }}>
      {isLoading ?
        <Text>
          Please Wait ...
        </Text>
        :
        <>
          <View style={{ flex: 0.35, backgroundColor: '#F76951', }}>
            <View style={{ flexDirection: 'row', marginTop: 50, alignItems: 'center', justifyContent: 'center' }}>
              <TouchableOpacity
                onPress={() => props.navigation.goBack()}
                style={styles.buttonStyle}>
                <Image
                  style={styles.backArrow}
                  source={require("../../images/back.png")}
                />
              </TouchableOpacity>
              <Text style={styles.titleText}>
                My Profile
              </Text>
            </View>
          </View>
          <View style={{ top: -50, flex: 0.9, alignItems: 'center', }}>
            <Image
              style={styles.rectangle}
              source={require("../../images/Rectangle.png")}
            />
            <View style={{
              position: 'absolute',
              alignItems: 'center',
              width: "100%",
            }}>
              <View style={styles.userProfileContiner}>
                <Image
                  style={{ height: 140, width: 140, borderRadius: 140 }}
                  // source={{ uri: userInfo.user_profile }}
                  source={{ uri: "https://i.pinimg.com/originals/fe/1f/4e/fe1f4e39feec16bbfbcf4d02d148b6d9.png" }}
                />
                <View style={styles.userProfile}>
                  <Image
                    style={styles.camera}
                    source={require("../../images/camera.png")}
                  />
                </View>
              </View>
              <View style={{ position: 'absolute', top: 120, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.userName}>
                  {userInfo.firstname + " " + userInfo.lastname}
                </Text>
                <Text style={styles.detailText}>
                  {userInfo.email}
                </Text>
              </View>
              <View style={[styles.userInformation, { marginTop: 40 }]}>
                <View style={styles.userContainer}>
                  <Text style={styles.addressText}>Address</Text>
                  <Image
                    style={styles.icon}
                    source={require("../../images/placeholder.png")}
                  />
                </View>
                <Text style={[styles.detailText, { marginTop: 5, width: "75%" }]}>{userInfo.street_address}</Text>
                {/* <Text style={styles.detailText}>{userInfo.phone_number} </Text> */}
              </View>
              <View style={styles.userInformation}>
                <View style={styles.userContainer}>
                  <Text style={styles.addressText}>Contact</Text>
                  <Image
                    style={styles.icon}
                    source={require("../../images/call.png")}
                  />
                </View>
                <Text style={[styles.detailText, { marginTop: 5 }]}>{userInfo.phone_number}</Text>
              </View>
            </View>

          </View>
        </>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  buttonStyle: {
    position: 'absolute',
    left: 20,
  },
  backArrow: {
    width: 20,
    height: 20
  },
  titleText: {
    color: 'white',
    fontSize: 20,
    fontWeight: '700'
  },
  rectangle: {
    height: "100%",
    width: "100%"
  },
  userProfileContiner: {
    borderWidth: 5,
    borderColor: "#ffffff",
    top: -60, width: 150, height: 150, borderRadius: 150,
    backgroundColor: '#d2d2d2'
  },
  userProfile: {
    alignItems: 'center',
    justifyContent: 'center',
    right: 0,
    position: 'absolute',
    bottom: 0, borderRadius: 40, width: 40, height: 40, backgroundColor: "#838383"
  },
  camera: {
    width: 25,
    height: 25,
  },
  userName: {
    color: 'black',
    fontSize: 22,
    fontWeight: '500'
  },
  detailText: {
    color: 'black',
    fontSize: 15,
    fontWeight: '300'
  },
  userInformation:
    { borderRadius: 20, padding: 20, marginTop: 20, alignSelf: 'flex-start', marginHorizontal: 20, backgroundColor: 'white', width: '90%', },
  userContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  icon: {
    width: 20,
    height: 20,
  },
  addressText: {
    color: 'black',
    fontSize: 18,
    fontWeight: '500'
  }
})

const mapStateToProps = (state) => {
  console.log("STATE", state.user.userData.Users)
  return { userInfo: state.user.userData.Users };
}

// export default Profile
export default connect(mapStateToProps, null)(Profile);
