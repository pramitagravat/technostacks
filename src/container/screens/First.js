import React, { useEffect, useState } from 'react'
import { View, StyleSheet, SafeAreaView, TouchableOpacity, Text, TextInput } from 'react-native'

const First = ({ navigation }) => {
    const [number, setNumber] = useState("")
    return (
        <SafeAreaView style={styles.container}>
            <View style={{ width: 100 }}>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => setNumber(text)}
                    value={number}
                    placeholder="Number"
                    keyboardType="numeric"
                />
            </View>
            <TouchableOpacity
                style={styles.buttonView}
                onPress={() => navigation.navigate("Second", { no: number })}>
                <Text style={styles.buttonText}>
                    Generate Button
                </Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        alignSelf: 'center',
        textAlign: 'center',
        borderWidth: 1,
        height: 30,
        color: "black",
        width: "100%",
    },
    buttonView: { padding: 10, marginTop: 50, borderWidth: 1, borderRadius: 10 },
    buttonText: { fontSize: 18, fontWeight: "300" },
})
export default First;