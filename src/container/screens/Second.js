import React, { useEffect, useState } from 'react'
import { View, StyleSheet, SafeAreaView, TouchableOpacity, Text, Image, ScrollView } from 'react-native'

const Second = (props) => {
    const [isLoading, setIsLoading] = useState(true)
    const [number, setNumber] = useState(props.route.params.no)
    const [blueButton, setBlueNumber] = useState()
    const [buttonStyle, setButtonStyle] = useState([])

    useEffect(() => {
        randomBlueButton(number)
        setIsLoading(false)
    }, [])

    const randomBlueButton = (no) => {
        let obj = []
        let randomNumber = Math.floor(Math.random() * no)
        for (var i = 0; i < no; i++) {
            obj.push({ id: i, white: true, blue: randomNumber == i ? true : false, red: false })
        }
        setBlueNumber(randomNumber)
        setButtonStyle(obj)
    }

    const setRed = (index) => {
        const newArray = [...buttonStyle]

        newArray.filter((val, i) => val.blue ? (val.red = true, val.blue = false) : null)
        setButtonStyle(newArray)
        var item = newArray[Math.floor(Math.random() * newArray.length)];
        newArray.filter((val, i) => val.id == item.id && val.white ? val.blue = true : null)
        setButtonStyle(newArray)
    }

    const returnColor = (index) => {
        const white = "white"
        const red = "red"
        const blue = "blue"
        return buttonStyle.map((value, id) => {
            if (value.blue) {
                return blue
            }
            else if (value.red) {
                return red
            }
            else {
                return white
            }
        })

    }

    const renderButtons = () => {
        const views = [];
        let color = returnColor()
        for (var i = 0; i < number; i++) {
            views.push(
                <TouchableOpacity
                    disabled={!buttonStyle[i].blue}
                    // disabled={blueButton == i ? false : true}
                    onPress={() => setRed(blueButton)}
                    style={[styles.buttonView, { backgroundColor: color[i] }]}>
                    <Text style={styles.buttonText}>
                        BTN {i}
                    </Text>
                </TouchableOpacity>
            );
        }
        return <View style={{ flexDirection: 'row', flexWrap: "wrap" }}>{views}</View>;
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flexDirection: 'row', margin: 10, }}>
                <TouchableOpacity
                    onPress={() => props.navigation.goBack()}
                    style={styles.buttonStyle}>
                    <Image
                        style={styles.backArrow}
                        source={require("../../images/back.png")}
                    />
                </TouchableOpacity>
            </View>
            {isLoading ?
                <View><Text>
                    Loading...
                </Text></View> :
                <ScrollView>
                    {renderButtons()}
                </ScrollView>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    buttonStyle: {
        marginHorizontal: 20,
    },
    backArrow: {
        tintColor: 'black',
        width: 20,
        height: 20
    },
    input: {
        alignSelf: 'center',
        textAlign: 'center',
        borderWidth: 1,
        height: 30,
        color: "black",
        width: "100%",
    },
    buttonView: { padding: 10, margin: 5, borderWidth: 1, borderRadius: 10 },
    buttonText: { fontSize: 18, fontWeight: "300" },
})
export default Second;