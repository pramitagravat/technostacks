import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native';

const Home = ({ navigation }) => {
    useEffect(() => {
    }, []);

    return (
        <SafeAreaView style={styles.mainContainer}>
            <TouchableOpacity
                onPress={() => navigation.navigate("Login")}
                style={styles.buttonView}>
                <Text style={styles.buttonText}>
                    Task 1
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => navigation.navigate("First")}
                style={styles.buttonView}>
                <Text style={styles.buttonText}>
                    Task 2
                </Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    mainContainer: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    buttonView: { width: 100, height: 50, backgroundColor: '#F76951', borderRadius: 10, justifyContent: 'center', margin: 10 },
    buttonText: { alignSelf: 'center', color: '#ffffff', fontSize: 20, fontWeight: '500' }
})

export default Home;
