import React, { useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Platform, TextInput, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')
import { loginData } from '../../utils/service';
import { loginAction } from '../../action/authAction'
import { connect } from 'react-redux';
const Login = (props) => {

  const [url, setUrl] = useState("")
  const [name, setName] = useState("")
  const [password, setpassword] = useState("")

  useEffect(() => {
  }, []);

  const checkValidation = () => {
    console.log("Platform", Platform.OS)
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (url == "") {
      alert("Please Enter URL")
    }
    else if (name == "") {
      alert("Please Enter User Email")
    }
    else if (!reg.test(name)) {
      alert("Please Enter Proper Email")
    }
    else if (password == "") {
      alert("Please Enter Password")
    }
    else {
      let userData = {
        "username": name,
        "password": password,
        "url": url,
        "multiple_user_login": {
          "device_token": "dMChrtBklsU:APA91bGv-",
          "device_type": Platform.OS,
          "dedevicevice_model": "Nokia 4.2",
          "_version": "10",
          "app_version": "2.0",
          "device_name": "Nokia",
          "device_uid": "f123447630d7c358"
        }
      }
      props.loginAction(userData).then(res => {
        if (res.code !== 200) {
          alert(res.message)
        }
        else {
          props.navigation.navigate("Profile")
        }
      })
    }
  }

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={{ flex: 0.3 }} />
      <View style={{ flex: 0.7, backgroundColor: '#F76951' }} />
      <View style={styles.container}>
        <View style={styles.childView}>
          <Image
            style={styles.logo}
            source={require("../../images/app_logo.png")}
          />
          <View style={{ marginTop: "10%" }}>
            <View style={styles.textInput}>
              <Image
                style={styles.icon}
                source={require("../../images/link.png")}
              />
              <TextInput
                style={styles.input}
                onChangeText={(text) => setUrl(text)}
                value={url}
                placeholder="URL"
                keyboardType="default"
              />
            </View>
            <View style={styles.textInput}>
              <Image
                style={styles.icon}
                source={require("../../images/user.png")}
              />
              <TextInput
                style={styles.input}
                onChangeText={(text) => setName(text)}
                value={name}
                placeholder="Username"
                keyboardType="default"
              />
            </View>
            <View style={styles.textInput}>
              <Image
                style={styles.icon}
                source={require("../../images/padlock.png")}
              />
              <TextInput
                style={styles.input}
                onChangeText={(text) => setpassword(text)}
                value={password}
                placeholder="Password"
                keyboardType="default"
                secureTextEntry={true}
              />
            </View>
            <View style={styles.boxContiner}>
              <Image
                style={styles.boxIcon}
                source={require("../../images/box.png")}
              />
              <Text style={[styles.checkBoxText, { marginHorizontal: 10 }]}>
                Remember me
              </Text>
              <View style={{ position: 'absolute', right: 1, }}>
                <Text style={[styles.checkBoxText, { color: "#F76951" }]}>
                  Forgot Password?
                </Text>
              </View>
            </View>
            <View style={styles.boxContiner}>
              <Image
                style={styles.boxIcon}
                source={require("../../images/box.png")}
              />
              <Text style={styles.checkBoxText}>
                I accept Terms & Condition
              </Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                // navigation.navigate("Profile")
                checkValidation()
              }
              style={styles.buttonView}>
              <Text style={styles.buttonText}>
                Sign In
              </Text>
            </TouchableOpacity>
            <Text style={styles.textStyle}>
              Privacy Policy {" "}
              <Text style={[styles.textStyle, { color: 'black' }]}>
                and
              </Text> Terms & Condition
            </Text>
          </View>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    borderWidth: 2, margin: 10, padding: 10, borderColor: '#f5f5f5',
    backgroundColor: '#ffffff',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.05,
    // shadowRadius: 4.65,
    elevation: 1,
    backgroundColor: '#FFFFFF',
    marginTop: "20%", width: "90%", height: "70%", position: 'absolute', alignSelf: 'center', alignItems: 'center'
  },
  childView: { justifyContent: 'center', alignItems: 'center', },
  logo: {
    marginTop: "10%",
    height: 100,
    width: 100
  },
  icon: {
    marginHorizontal: 10,
    width: 25,
    height: 25,
  },
  boxIcon: {
    tintColor: "#d5d5d5",
    width: 20,
    height: 20,
  },
  textInput: {
    marginTop: 20,
    width: width * 0.8, height: 50, borderRadius: 5,
    borderColor: "#d2d2d2",
    borderWidth: 1.5, flexDirection: 'row', alignItems: 'center'
  },
  input: {
    width: "100%",
  },
  boxContiner: { flexDirection: 'row', alignItems: 'center', marginTop: 10 },
  checkBoxText: { marginHorizontal: 10, color: "#301C30", fontWeight: '300' },
  buttonView: { alignItems: 'center', alignSelf: 'center', width: 100, height: 50, backgroundColor: '#F76951', borderRadius: 10, justifyContent: 'center', marginTop: 40 },
  buttonText: { alignSelf: 'center', color: '#ffffff', fontSize: 20, fontWeight: '500' },
  textStyle: {
    marginTop: 20,
    alignSelf: 'center',
    color: "#F76951", fontWeight: '300'
  }
})

export default connect(null, { loginAction })(Login)
