import React from 'react';
import { View, Text } from 'react-native'
import { Provider } from 'react-redux'
import configureStore from './src/store/store'
import MyStack from './src/container/navigator/navigation'
const App = () => {
  const store = configureStore;
  // console.log("configureStore", store)
  return (
    <Provider store={store}>
      <MyStack />
    </Provider>

  )
}
export default App;